
Automated Tests
---------------
Since the instruction for the simulated events are comming from a separate thread, the tests are very sensible
to timing and can generate false negatives. For this reason, in some location of the code, there are sleeping periodes that have been added to allow time for events to be processed and for results to be generated.

All test must be over in 20 seconds or less.

Description of tests:
---------------------
graphics_display_states:
	Test the graphics display states after it has been created.

empty_window button_xtest:
	Display an empty window and close it after 3 seconds.

button_xtest:
	Using XTest, love the mouse around the boundaries of a button view and simulate click and drag mouse events. The program close itself after a delay.

mouse_events_test:
	Test the following mouse events:
		- mouse down
		- mouse up
		- mouse click
		- mouse double-click
		- mouse drag
		- mouse enter
		- mouse leave
		- mouse move


mouse_buttons_tests:
	Make sure that only physical mouse button 1, 2 and 3 generate mouse-down and mouse-up events.
	The artificial button event 4 and 5 should not generate mouse-down or mouse-up events.
	
hgrid_key_navigation_test
  Make sure that the key navigation works well in a GridHLayout. 
  
hlayout_key_navigation_test
  Make sure that the key naviagation works well in a HLayout.
  
vlayout_key_navigation_test
  Make sure that the key navigation works well in a VLayout.

focus_on_mouse_down_test
  Make sure that AcceptKeyNavFocusOnMouseDown works well.


