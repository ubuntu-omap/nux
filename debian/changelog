nux (2.0.0+bzr20120120+550+8+201202020059~oneiric1+ti2) oneiric; urgency=low

  * Fixed sgx dependencies

 -- Frederic Plourde <frederic.plourde@collabora.co.uk>  Sat, 25 Feb 2012 10:48:09 -0500

nux (2.0.0+bzr20120120+550+8+201202020059~oneiric1+ti1) oneiric; urgency=low

  * New upstream release
  * Fixed armel architecture and sgx dependencies

 -- Frederic Plourde <frederic.plourde@collabora.co.uk>  Thu, 23 Feb 2012 16:55:10 -0500

nux (2.0.0+bzr20120120+550+8+201202020059~oneiric1) oneiric; urgency=low

  * Auto build.

 -- Ricardo Salveti <rsalveti@rsalveti.net>  Thu, 02 Feb 2012 00:59:34 +0000

nux (2.0.0+bzr20120120-0linaro2) UNRELEASED; urgency=low

  * Enable build for OpenGL ES 2.0 for all archs

 -- Ricardo Salveti de Araujo <ricardo.salveti@linaro.org>  Tue, 31 Jan 2012 02:33:56 -0200

nux (2.0.0+bzr20120120-0linaro1) oneiric; urgency=low

  * New upstream snapshot release
  * debian/control: fixing dependencies for OpenGL ES 2.0 enabled build
  * debian/rules: enabling build for opengles 2.0 for armel|armhf
  * debian/rules: enabling parallel build
  * Enabling autogen.sh for dev upstream snapshot builds

 -- Ricardo Salveti de Araujo <ricardo.salveti@linaro.org>  Sun, 22 Jan 2012 22:06:10 -0200

nux (2.0.0-0ubuntu2) precise; urgency=low

  * Build with glew1.6, the issue it was creating on intel previous cycles
    seem to be sorted so there is no reason to stay on an old version

 -- Sebastien Bacher <seb128@ubuntu.com>  Thu, 19 Jan 2012 17:24:19 +0100

nux (2.0.0-0ubuntu1) precise; urgency=low

  [ Aurélien Gâteau ]
  * New upstream release:
    - compiz crashed with SIGSEGV in nux::ROProperty<std::string>::operator
      std::string() (LP: #881106)
    - compiz crashed with SIGSEGV in
      nux::WindowCompositor::FindKeyFocusAreaFrom() (LP: #860852)
    - compiz crashed with SIGSEGV in nux::Area::FindKeyFocusArea()
      (LP: #848952)
    - compiz crashed with SIGILL in typeinfo for std::ostream() (LP: #849881)
    - compiz crashed with SIGSEGV in nux::WindowThread::IsInsideLayoutCycle()
      (LP: #864686)
    - Select quicklist items with just one right click (LP: #688830)
    - OnMouseDoubleClick in InputArea isn't working (LP: #708020)
    - Menubar - horizontal scroll over a menu opens it (LP: #775872)
    - Clicking on a dash category header should not give it keyboard focus
      (LP: #905921)
    - Horizontal scroll on maximize/restore window control restores window.
      (LP: #809717)
    - Horizontally scrolling while highlighting an item in the dash or on an
      indicator will open the item. (LP: #837004)
    - compiz crashed with SIGSEGV in free() (LP: #864446)
    - Mouse wheel doesn't works if the mouse pointer is inside dash scrollbar.
      (LP: #888819)
  * debian/rules, debian/control:
    - add nuxabiversion capability as upstream is breaking a lot the ABI
      wasn't bumping the soname. Create a virtual package similar to what
      we already do in compiz to ensure we dep on the right version.
      Remove the makeshlibs override then as not needed anymore.
    - bump to nux 2.0 with new libnux-2.0-0, libnux-2.0-common, libnux-2.0-dev
      packages. Make the necessary changes in .install files as well
    - don't ship anymore the -doc package.
  * debian/nux-tools.install:
    - move the apport file there instead of the -common package to enable
      co-instability

nux (1.16.0.2011.10-0linaro2+ti6) oneiric; urgency=low

  * Fixed target architecture to armel only

 -- Frederic Plourde <frederic.plourde@collabora.co.uk>  Wed, 04 Jan 2012 10:19:07 -0500

nux (1.16.0.2011.10-0linaro2+ti5) oneiric; urgency=low

  * debian/changelog 

 -- Frederic Plourde <frederic.plourde@collabora.co.uk>  Wed, 04 Jan 2012 09:46:21 -0500

nux (1.16.0.2011.10-0linaro2+ti4) oneiric; urgency=low

  * corrected debian/changelog

 -- Frederic Plourde <frederic.plourde@collabora.co.uk>  Thu, 22 Dec 2011 11:17:02 -0500

nux (1.16.0.2011.10-0linaro2+ti3) oneiric; urgency=low

  * Changed debian/control to depend on TI's pvr gles libs 

 -- frederic Plourde <fredinfinite23@panda23>  Tue, 20 Dec 2011 19:36:20 -0500

nux (1.16.0.2011.10-0linaro2+ti1) oneiric; urgency=low

  * Saving/restoring nux::WindowThread::RenderInterfaceFromForeignCmd FBOs
    before/after rendering interface to nux texture. This lets compiz wrap
    unity inside its own FBO.

 -- Frederic Plourde <frederic.plourde@collabora.co.uk>  Thu, 15 Dec 2011 14:33:36 -0500

nux (1.16.0.2011.10-0linaro2) oneiric; urgency=low
 -- Didier Roche <didrocks@ubuntu.com>  Fri, 13 Jan 2012 09:04:48 +0100

nux (1.16.0-0ubuntu2) precise; urgency=low

  * Don't build anymore the documentation and don't gputests files
  * debian/control:
    - add build-dep on libutouch-geis-dev

 -- Didier Roche <didrocks@ubuntu.com>  Tue, 22 Nov 2011 18:55:04 +0100

nux (1.16.0-0ubuntu1) oneiric-proposed; urgency=low

  * New upstream release.
    - Regression: shift+click on a launcher icon to open a new application
      instance gone (part 1) (LP: #754565)
    - Fix misc rendering issues and double ref

 -- Didier Roche <didrocks@ubuntu.com>  Mon, 17 Oct 2011 09:10:05 +0200

nux (1.14.0-0ubuntu1) oneiric; urgency=low

  * New upstream release.
    - compiz crashed with SIGSEGV in nux::WindowCompositor::FindKeyFocusArea()
      (LP: #845873)
    - compiz crashed with SIGSEGV in sigc::signal_base::impl() (LP: #831769)
    - can't maximize windows on second monitor and Qt windows displayed in
      wrong place (LP: #861341)
    - Dash - Dash should support drag and drop to external applications
      (LP: #857431)
    - Dash - When the Dash is re-opened and statefully displaying a previous
      query, it should be possible to add to the query by pressing the 'right
      arrow cursor key' (LP: #841828)
    - blacklist old geforce fx cards for unity-3d (LP: #769402)
  * debian/control:
    - build-dep on libxinerama-dev

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 29 Sep 2011 18:46:45 +0200

nux (1.12.0-0ubuntu1) oneiric; urgency=low

  * New upstream release.
    - valgrind: Mismatched free in libnux (LP: #785118)
    - other misc rendering optimizations

 -- Didier Roche <didrocks@ubuntu.com>  Mon, 26 Sep 2011 12:31:59 +0200

nux (1.10.0-0ubuntu1) oneiric; urgency=low

  * New upstream release.
    - compiz crashed with SIGSEGV in nux::GpuDevice::GetGpuInfo()
      (LP: #765375)
    - compiz crashed with SIGSEGV in nux::WindowCompositor::SetKeyFocusArea()
      (LP: #846059)
    - unity_support_test crashed with SIGSEGV in fclose() (LP: #843369)
    - compiz crashed with SIGSEGV in nux::Area::FindKeyFocusArea()
      (LP: #815114)

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 15 Sep 2011 18:42:58 +0200

nux (1.8.0-0ubuntu1) oneiric; urgency=low

  * New upstream release.
    - Unity crashes if you open a quicklist and then the corresponding
      launcher icon closes (LP: #801413)
  * debian/control:
    - bump shlib
    - remove shlibs restriction (no more ABI breakage until finale)

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 08 Sep 2011 20:06:12 +0200

nux (1.6.0-0ubuntu2) oneiric; urgency=low

  * tools/unity_support_test.c:
    - enables caching startup result
  * debian/50_check_unity_support, debian/nux-tools.install:
    - install new helper to check for unity support before logging into
      session (LP: #842656)

 -- Didier Roche <didrocks@ubuntu.com>  Tue, 06 Sep 2011 17:53:03 +0200

nux (1.6.0-0ubuntu1) oneiric; urgency=low

  * New upstream release
  * debian/rules:
    - updated shlibs

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 01 Sep 2011 17:32:57 +0200

nux (1.4.0-0ubuntu3) oneiric; urgency=low

  * Backport a small api tweak from trunk, it's a small abi break but required
    to fix an unity issue and better now than latter
  * debian/control: break on current unity, it will need a rebuild
  * debian/rules: updated shlibs

 -- Sebastien Bacher <seb128@ubuntu.com>  Thu, 25 Aug 2011 23:03:03 +0200

nux (1.4.0-0ubuntu2) oneiric; urgency=low

  * Backport upstream revision to fix the opening effect of the unity dash

 -- Sebastien Bacher <seb128@ubuntu.com>  Thu, 25 Aug 2011 18:53:09 +0200

nux (1.4.0-0ubuntu1) oneiric; urgency=low

  * New upstream version:
    - "scrolling down in a lens brings it back to the top automatically" 
      (lp: #821534)
  * debian/rules: updated shlib

 -- Sebastien Bacher <seb128@ubuntu.com>  Thu, 25 Aug 2011 13:42:45 +0200

nux (1.2.2-0ubuntu2) oneiric; urgency=low

  * The package claims to be compliant to C++0x, but happily uses
    old macros. Fix it.

 -- Matthias Klose <doko@ubuntu.com>  Fri, 19 Aug 2011 19:08:29 +0200

nux (1.2.2-0ubuntu1) oneiric; urgency=low

  * New upstream release.
  * debian/rules: updated shlib

 -- Sebastien Bacher <seb128@ubuntu.com>  Thu, 18 Aug 2011 17:57:09 +0200

nux (1.2.0-0ubuntu1) oneiric; urgency=low

  * New upstream release.
  * debian/rules:
    - bump shlib

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 11 Aug 2011 18:29:22 +0200

nux (1.0.8-0ubuntu3) oneiric; urgency=low

  * debian/control:
    - transition to boost 1.46

 -- Didier Roche <didrocks@ubuntu.com>  Wed, 10 Aug 2011 11:33:35 +0200

nux (1.0.8-0ubuntu2) oneiric; urgency=low

  * Cherry-pick upstream:
    - compiz crashed with SIGSEGV in nux::IOpenGLVertexShader
      ::IOpenGLVertexShader() (LP: #819739)

 -- Didier Roche <didrocks@ubuntu.com>  Tue, 02 Aug 2011 16:51:02 +0200

nux (1.0.8-0ubuntu1) oneiric; urgency=low

  * New upstream release.
  *  debian/rules:
    - bump shlib

 -- Didier Roche <didrocks@ubuntu.com>  Mon, 01 Aug 2011 19:52:12 +0200

nux (1.0.6-0ubuntu1) oneiric; urgency=low

  * New upstream release.
  * debian/control:
    - dep on libglu1-mesa-dev
  * debian/rules:
    - bump shlib

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 21 Jul 2011 18:41:46 +0200

nux (1.0.4-0ubuntu1) oneiric; urgency=low

  * New upstream release.
  * debian/rules:
    - bump shlib

 -- Didier Roche <didrocks@ubuntu.com>  Mon, 04 Jul 2011 18:34:48 +0200

nux (1.0.2-0ubuntu1) oneiric; urgency=low

  * New upstream release.
  * Cherry-pick a fix for FTBFS with -fpermissive
  * debian/control:
    - add new libxdamage-dev and libxcomposite-dev build-dep
    - add new libboost1.42-dev dep as well, should be transitionned to 1.46 once
      compiz is transitionned as well
  * remove debian/patches/01_build_with_gcc46.patch as included upstream
  * debian/rules:
    - disable google code tests while building
  * debian/control, debian/rules, debian/libnux-1.0-common.install,
    debian/libnux-1.0-dev.install, debian/libnux-1.0-doc.install,
    debian/libnux-1.0-0.install:
    - change, prepare next ABI breakage and remove no more needed Breaks: with
      new soname bump
  * libnux-1.0-common now replaces: libnux-0.9-common for the apport hook

 -- Didier Roche <didrocks@ubuntu.com>  Wed, 22 Jun 2011 17:16:16 +0200

nux (0.9.48-0ubuntu4) oneiric; urgency=low

  * Cherry-pick more fixes:
    - input characters become invisible on switching dash to fullscreen mode
      (LP: #758248)
    - Fixed memory leak. Related to bug (LP: #758248)
    - Fix compiz crashed with SIGSEGV in nux::NThreadSafeCounter::Decrement()
      (LP: #763225)

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 26 May 2011 17:01:57 +0200

nux (0.9.48-0ubuntu3) oneiric; urgency=low

  * Cherry-pick fix for launcher sometimes doesn't hide when there are
    windows beneath it (LP: #772185)

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 26 May 2011 15:11:53 +0200

nux (0.9.48-0ubuntu2) oneiric; urgency=low

  * debian/patches/01_build_with_gcc46.patch:
    - Fix Fails to compile with GCC 4.6 (thanks Adam Williamson) (LP: #745392)
  * debian/control, rules, patches/series:
    - add quilt infrastructure

 -- Didier Roche <didrocks@ubuntu.com>  Wed, 18 May 2011 13:13:49 +0200

nux (0.9.48-0ubuntu1) natty-proposed; urgency=low

  * New upstream release.
    - fix compiz crashed with SIGSEGV in nux::IOpenGLShaderProgram::Begin()
      (LP: #768178)
    - launcher loses focus (LP: #763883)

 -- Didier Roche <didrocks@ubuntu.com>  Wed, 27 Apr 2011 09:32:26 +0200

nux (0.9.46-0ubuntu4) natty; urgency=low

  * Cherry-pick one fix to avoid wrong MouseMove event triggering false
    wrong Launcher intellhide behavior (LP: #767250)

 -- Didier Roche <didrocks@ubuntu.com>  Wed, 20 Apr 2011 20:07:01 +0200

nux (0.9.46-0ubuntu3) natty; urgency=low

  * Cherry-pick blacklisting additional driver + UNITY_FORCE_START env variable
    to bypass unity/compiz testing:
    - Partial screen corruption and poor performance on GeForce 6150
      (LP: #764379)
    - [nvidia, 7300, 7400] display freeze when using unity desktop
      (LP: #728745)

 -- Didier Roche <didrocks@ubuntu.com>  Wed, 20 Apr 2011 18:08:40 +0200

nux (0.9.46-0ubuntu2) natty; urgency=low

  * Cherry-pick:
    - fix for "does not display icons until hovered" on old ATI hardware
      (LP: #726033)

 -- Didier Roche <didrocks@ubuntu.com>  Wed, 20 Apr 2011 10:06:03 +0200

nux (0.9.46-0ubuntu1) natty; urgency=low

  * New upstream release.
    - ensure text entry size is updated with different font size
    - fix some missing nux events sent (LP: #763275, #750122)

 -- Didier Roche <didrocks@ubuntu.com>  Tue, 19 Apr 2011 19:26:25 +0200

nux (0.9.44-0ubuntu2) natty; urgency=low

  * Cherry-pick a fix for black text is on a black background when item is
    selected in dash search filter combobox (LP: #761201)

 -- Didier Roche <didrocks@ubuntu.com>  Fri, 15 Apr 2011 12:46:01 +0200

nux (0.9.44-0ubuntu1) natty; urgency=low

  * New upstream release.
    - compiz crashed with SIGSEGV in nux::WindowCompositor::RenderTopViews()
      (LP: #754461)
    - compiz crashed with SIGSEGV in nux::Area::InitiateResizeLayout()
      (LP: #757709)
    - Add ATI Radeon Mobility 7500 to compiz blacklist (LP: #760687)
    - Letters cut off at the end in the applications window  (LP: #753083)

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 14 Apr 2011 22:02:48 +0200

nux (0.9.42-0ubuntu1) natty; urgency=low

  * New upstream release.
    - compiz crashed with SIGSEGV in nux::XInputWindow::NativeHandleList()
      (LP: #753110)
    - compiz crashed with SIGSEGV in nux::WindowCompositor::ProcessEvent()
      (LP: #753005)
    - compiz crashed with SIGSEGV in nux::WindowCompositor::RenderTopViews()
      (LP: #754461)
  * Breaks on current unity:
    - debian/control: update breaks statement
    - debian/rules: bump the shlib

 -- Didier Roche <didrocks@ubuntu.com>  Mon, 11 Apr 2011 12:41:58 +0200

nux (0.9.40-0ubuntu1) natty; urgency=low

  * New upstream release.
    - [fglrx] compiz crashed with SIGSEGV in nux::IOpenGLSurface::UnlockRect()
      (LP: #685682)
    - compiz crashed with SIGSEGV in
      nux::WindowCompositor::ViewWindowPreEventCycle() (LP: #741952)
    - Missing icons in app launcher (LP: #728393)
    - compiz crashed with SIGSEGV in malloc_consolidate() (LP: #743738)
    - compiz crashed with SIGSEGV in nux::BaseWindow::IsModal() (LP: #749675)
    - compiz crashed with SIGSEGV in
      nux::GpuRenderStates::SubmitChangeStates() (LP: #722391)
    - compiz crashed with SIGSEGV in nux::InputArea::StartDragAsSource()
      (LP: #737519)
    - unity_support_test crashed with SIGSEGV in
      nux::IOpenGLAsmVertexShader::IOpenGLAsmVertexShader() (LP: #734519)
    - compiz crashed with SIGSEGV in nux::WindowCompositor::RenderTopViews()
      (LP: #747486)
  * debian/copyright:
    - licence is now LGPL2.1+
  * ABI break, add Breaks: unity (<< 3.8.4)
  * debian/rules:
    - bump the shlib

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 07 Apr 2011 18:26:13 +0200

nux (0.9.38-0ubuntu1) natty; urgency=low

  * New upstream release.
    - compiz crashed with SIGSEGV in std::_List_node_base::_M_hook()
      (LP: #711916)
    - compiz crashed with SIGSEGV in nux::View::PostProcessEvent2()
      (LP: #712498)
    - compiz crashed with SIGSEGV in sigc::internal::signal_emit0<void,
      sigc::nil>::emit() (LP: #729715)
    - compiz crashed with SIGSEGV in brw_validate_state() (LP: #730707)
    - Finish Nux detection tool support (LP: #722089)
    - compiz crashed with SIGABRT in raise() (LP: #741942)
    - compiz crashed with SIGSEGV in PanelMenuView::ProcessEvent()
      (LP: #742139)
    - compiz crashed with SIGSEGV in operator->() (LP: #741961)
    - Graphics hardware capability detection (LP: #685208)
    - compiz crashed with SIGSEGV in nux::GraphicsDisplay::GrabPointer()
      (LP: #740784)
    - compiz crashed with SIGSEGV in nux::Area::InitiateResizeLayout()
      (LP: #741977)
    - compiz crashed with SIGSEGV in nux::GraphicsDisplay::GrabKeyboard()
      (LP: #742233)
    - Missing child addition signals on nux::Layout and nux::View
      (LP: #734803)
    - compiz crashed with SIGSEGV in
      nux::GpuRenderStates::SubmitChangeStates() (LP: #735908)
    - unity_support_test crashed with SIGSEGV (LP: #743848)
  * debian/control:
    - adding libpci-dev build-dep for the new lighter unity_support_tool

 -- Didier Roche <didrocks@ubuntu.com>  Fri, 01 Apr 2011 11:15:39 +0200

nux (0.9.36-0ubuntu1) natty; urgency=low

  * New upstream release.
    - compiz crashed with SIGSEGV in nux::BaseTexture::GetDeviceTexture()
      (LP: #692823)
    - compiz crashed with SIGSEGV in nux::Focusable::SetFocused()
      (LP: #737716)
    - compiz crashed with SIGSEGV in nux::Memcpy() (LP: #738225)
  * debian/rules:
    - bump shlibs

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 24 Mar 2011 04:54:54 +0100

nux (0.9.34-0ubuntu1) natty; urgency=low

  * New upstream release.
    - compiz crashed with SIGSEGV in nux::WindowCompositor::ResetDnDArea()
      (LP: #728785)
    - unity_support_test crashed with SIGSEGV in
      nux::GpuDevice::GetOpenGLMajorVersion() (LP: #709222)
  * debian/source_nux.py, debian/libnux-0.9-common.install:
    - install an apport hook (mainly for the unity_support_test tool) reporting
      xorg info
  * debian/rules:
    - bump shlibs

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 17 Mar 2011 17:55:17 +0100

nux (0.9.32-0ubuntu1) natty; urgency=low

  * New upstream release.
    - compiz crashed with SIGSEGV in std::basic_string<char,
      std::char_traits<char>, std::allocator<char> >::assign() (LP: #729412)
    - Unity interface not resized properly when external monitor is used
      (LP: #691772)
    - unity_support_test crashed with SIGSEGV in
      nux::IOpenGLAsmVertexShader::IOpenGLAsmVertexShader() (LP: #709649)
    - compiz crashed with SIGSEGV in
      nux::GpuRenderStates::SubmitChangeStates() (LP: #719156)
    - compiz crashed with SIGSEGV in
      nux::GpuRenderStates::SubmitChangeStates() (LP: #724903)
    - unity_support_test crashed with SIGSEGV in
      nux::IOpenGLAsmVertexShader::IOpenGLAsmVertexShader() (LP: #723531)
    - unity_support_test crashed with SIGSEGV in
      nux::IOpenGLAsmVertexShader::IOpenGLAsmVertexShader() (LP: #723158)
    - unity_support_test crashed with SIGSEGV in
      nux::IOpenGLAsmVertexShader::IOpenGLAsmVertexShader() (LP: #727895)
  * debian/rules:
    - bump shlib

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 10 Mar 2011 19:51:23 +0100

nux (0.9.30-0ubuntu1) natty; urgency=low

  * New upstream release:
    - compiz crashed with SIGSEGV in g_signal_connect_data@plt() (LP: #725827)
    - compiz crashed with SIGSEGV in nux::View::PostProcessEvent2()
      (LP: #726253)
    - Unity FTBFS on armel due to Nux (LP: #721118)
  * debian/rules:
    - bump shlib

 -- Didier Roche <didrocks@ubuntu.com>  Tue, 01 Mar 2011 14:20:24 +0100

nux (0.9.28-0ubuntu1) natty; urgency=low

  * New upstream release:
    - BaseWindow should notify change on the visible state (LP: #702702)
  * Cherry-picked:
    - Unity FTBFS on armel due to Nux (LP: #721118)
  * debian/rules:
    - bump shlib

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 24 Feb 2011 20:16:04 +0100

nux (0.9.26-0ubuntu1) natty; urgency=low

  * New upstream release.
    - unity_support_test crashed with SIGSEGV in
      nux::IOpenGLAsmVertexShader::IOpenGLAsmVertexShader() (LP: #711021)
    - cherry-pick additional upstream commit to fix build issues
  * debian/rules:
    - bump shlib

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 17 Feb 2011 20:37:21 +0100

nux (0.9.22-0ubuntu1) natty; urgency=low

  * New upstream release.
  * debian/rules:
    - bump shlib
  * debian/libnux-0.9-doc.install:
    - don't install examples, they are not shipped anymore

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 10 Feb 2011 20:45:14 +0100

nux (0.9.18-0ubuntu1) natty; urgency=low

  * New upstream release.
  * debian/rules:
    - bump shlib

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 27 Jan 2011 14:31:24 +0100

nux (0.9.16-0ubuntu1) natty; urgency=low

  * New upstream release.
  * debian/rules:
    - bump shlib

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 20 Jan 2011 19:02:56 +0100

nux (0.9.14-0ubuntu2) natty; urgency=low

  * debian/control:
    - make the -dev package depends on libpcre3-dev as well
  * fix in trunk to add it to the .pc file and merge to the packaging branch

 -- Didier Roche <didrocks@ubuntu.com>  Fri, 14 Jan 2011 21:28:40 +0100

nux (0.9.14-0ubuntu1) natty; urgency=low

  * New upstream release.
  * debian/control:
    - add libpcre3-dev
  * debian/rules:
    - bump shlib

 -- Didier Roche <didrocks@ubuntu.com>  Fri, 14 Jan 2011 20:45:57 +0100

nux (0.9.12-0ubuntu1) natty; urgency=low

  * New upstream release:
    - "compiz crashed with SIGSEGV in nux::IOpenGLSurface::UnlockRect()"
      is fixed (lp: #686698)
  * debian/rules:
    - updated the shlibs
  * debian/libnux-0.9-doc.install:
    - install the gputests examples there

 -- Sebastien Bacher <seb128@ubuntu.com>  Fri, 17 Dec 2010 13:59:57 +0100

nux (0.9.10-0ubuntu2) natty; urgency=low

  * debian/control:
    - force -common to be in sync with nux
    - add Vcs-Bzr

 -- Didier Roche <didrocks@ubuntu.com>  Mon, 13 Dec 2010 12:39:21 +0100

nux (0.9.10-0ubuntu1) natty; urgency=low

  * New upstream release.
   - Fix SEGSEGV in nux::ResourceData::GetResourceIndex() const ()
     (LP: #682345)
   - Add detection test software
  * debian/rules:
    - bump shlibs for new release
  * debian/nux-tools.install, debian/control:
    - add nux-tools to ship the detection test software

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 09 Dec 2010 19:56:53 +0100

nux (0.9.8-0ubuntu1) natty; urgency=low

  * New upstream release + cherry pick latest commit:
    - don't ship .bmp files but rather .png (LP: #678460)
    - Clicks outside of Quicklist don't close it (LP: #683079)
  * debian/rules:
    - bump shlibs for new release

 -- Didier Roche <didrocks@ubuntu.com>  Tue, 30 Nov 2010 17:32:33 +0100

nux (0.9.6-0ubuntu1) natty; urgency=low

  * New upstream release.
    - nux::Timeline + tests (LP: #676059)
  * debian/copyright:
    - updated to be parsable
  * debian/libnux-dev.install:
    - leftover, removed
  * debian/rules:
    - tests doesn't pass this week, will be fixed next.
    - bump shlibs

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 25 Nov 2010 19:46:27 +0100

nux (0.9.4-0ubuntu2) natty; urgency=low

  * removing debian/libnux-0.9-0.symbols and use a shlibs as there is no
    ABI/API stability + nux doesn't export the same symbols on every arch.

 -- Didier Roche <didrocks@ubuntu.com>  Fri, 19 Nov 2010 10:40:41 +0100

nux (0.9.4-0ubuntu1) natty; urgency=low

  * Initial packaging

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 18 Nov 2010 19:17:32 +0100
